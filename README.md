Тестовое задание на React
-------------------------

Установить зависимости **yarn** или **npm install**

Запуск проекта **yarn start** или **npm run start**

Сборка **yarn build** или **npm run build**

**DEMO: http://reactapp.ru/test-task/**

![Alt text](http://reactapp.ru/img/catfeed.jpg)
