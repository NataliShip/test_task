import React, { Component } from 'react';
import Card from './Card';
import './App.css';

//данные могут быть получены от бэкенда или через API
const products = [
  {
    taste: 'с фуа-гра',
    description: 10,
    giftAmount: '',
    gift: 'мышь в подарок',
    amount: '0,5',
    disabled: false,
    defaultText: 'Чего сидишь? Порадуй котэ ',
    selectedText: 'Печень утки разварная с артишоками.',
    disabledText: 'Печалька, с фуа-гра закончился.',
  },
  {
    taste: 'с рыбой',
    description: 40,
    giftAmount: 2,
    gift: ' мыши в подарок',
    amount: '2',
    disabled: false,
    defaultText: 'Чего сидишь? Порадуй котэ ',
    selectedText: 'Головы щучьи с чесноком да свежайшая сёмгушка.',
    disabledText: 'Печалька, с рыбой закончился.',
  },
  {
    taste: 'с курой',
    description: 100,
    giftAmount: 5,
    gift: ' мышей в подарок заказчик доволен',
    amount: '5',
    disabled: true,
    defaultText: 'Чего сидишь? Порадуй котэ ',
    selectedText: 'Филе из цыплят с трюфелями в бульоне.',
    disabledText: 'Печалька, с курой закончился.',
  }
];

class App extends Component {
  constructor() {
    super();
    this.state = {
      products: products,
    }
  }

  render() {
    return (
      <div className="app">
        <div className='app__wrap'>
          <div className="app__background">
            <h1 className="app__title">Ты сегодня покормил кота?</h1>
            <div className="app__card-container">
              {this.state.products.map((item, index) =>
              <Card cardContent={item} key={index} />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
