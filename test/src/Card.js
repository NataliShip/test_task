import React, {Component, Fragment} from 'react';
import './Card.css';

class Card extends Component {
  constructor() {
    super();
    this.state = {
      selected: false,
      hoverText: 'Сказочное заморское яство',
    }
  }

  render() {
    return (
    <div>
      {!this.props.cardContent.disabled ?
      (<Fragment>
        <div className={this.state.selected ? 'card card--selected' : 'card'}
             onClick={this.toggleSelect}
             onMouseOver={this.hover}
             onMouseLeave={this.leave}
        >
          <div className={'card__text-content'}>
            <div className={'card__title'}>{this.state.hoverText}</div>
            <div className={'card__brand'}>Нямушка</div>
            <div className={'card__taste'}>{this.props.cardContent.taste}</div>
            <div className={'card__desc'}><span className={'card__desc-number'}>{this.props.cardContent.description}</span> порций
            </div>
            <div className={'card__gift'}>
              <span className={'card__gift-amount'}>{this.props.cardContent.giftAmount}</span>
              {this.props.cardContent.gift}
            </div>
          </div>
          <div className={this.state.selected ? 'card__text-background card__text-background--selected' : 'card__text-background'}>
            <div className={'card__amount'}>
              <span className={'card__amount-number'}>
              {this.props.cardContent.amount}</span> кг
            </div>
          </div>
        </div>

        {!this.state.selected ?
        (<div className={'card__text'}>
          {this.props.cardContent.defaultText}
          <span className={'card__text-buy'} onClick={this.toggleSelect}>купи.</span>
        </div>):
        (<div className={'card__text'}>
          {this.props.cardContent.selectedText}
        </div>)
        }

      </Fragment>):

      (<Fragment>
        <div className={'card card--disabled'}>
          <div className={'card__text-content'}>
            <div className={'card__title card__title--disabled'}>Сказочное заморское яство</div>
            <div className={'card__brand card__brand--disabled'}>Нямушка</div>
            <div className={'card__taste card__taste--disabled'}>{this.props.cardContent.taste}</div>
            <div className={'card__desc card__desc--disabled'}><span className={'card__desc-number'}>{this.props.cardContent.description}</span> порций
            </div>
            <div className={'card__gift card__gift--disabled'}>
                <span className={'card__gift-amount card__gift-amount--disabled'}>{this.props.cardContent.giftAmount}</span>
                {this.props.cardContent.gift}
            </div>
          </div>
          <div className={'card__text-background card__text-background--disabled'}>
            <div className={'card__amount'}>
              <span className={'card__amount-number'}>
              {this.props.cardContent.amount}</span> кг
            </div>
          </div>
        </div>

        <div className={'card__text-disabled'}>
          {this.props.cardContent.disabledText}
        </div>
      </Fragment>)}
    </div>
    );
  }

  hover = () => {
    if (this.state.selected) {
      this.setState({hoverText: 'Котэ не одобряет?'});
    }
  }

  leave = () => {
    this.setState({hoverText: 'Сказочное заморское яство'});
  }

  toggleSelect = () => {
    if (this.state.selected === false) {
      this.setState({selected: true});
    } else {
      this.setState({selected: false});
      this.setState({hoverText: 'Сказочное заморское яство'});
    }
  }
}

export default Card;